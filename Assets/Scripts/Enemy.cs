using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
	[SerializeField] private bool killPlayer = true;
	[SerializeField] private bool capturePlayer = true;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.transform.tag.Equals("Player"))
		{
			if(killPlayer) GameManager.me.EnemyKilledPlayer();
			else if(capturePlayer) GameManager.me.EnemyCatchedPlayer();
		}
	}
}