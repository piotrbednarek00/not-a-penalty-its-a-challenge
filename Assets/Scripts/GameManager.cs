using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	public static GameManager me;

	public UnityEvent onGameWin;

	[SerializeField] private CanvasGroup endGame;
	[SerializeField] private CanvasGroup loseGame;

	[SerializeField] private int currentLevel = 0;
	[SerializeField] private int nextLevelNumber = 0;
	[SerializeField] private PlayerController playerController;

	private void Awake()
	{
		if(me == null)
			me = this;
	}

	private void OnDisable()
	{
		Destroy(this.gameObject);
	}

	public void EnemyKilledPlayer()
	{
		playerController.Death();
		loseGame.alpha = 1f;
		loseGame.interactable = true;
		loseGame.blocksRaycasts = true;
	}

	public void EnemyCatchedPlayer()
	{
		playerController.Catched();
		loseGame.alpha = 1f;
		loseGame.interactable = true;
		loseGame.blocksRaycasts = true;
	}

	public void FinishLevel()
	{
		playerController.StopMoving();
		endGame.alpha = 1f;
		endGame.interactable = true;
		endGame.blocksRaycasts = true;
		onGameWin.Invoke();
	}

	public void LoadNextLevel()
	{
		if(nextLevelNumber == -1)
			SceneManager.LoadScene("MainMenu");
		else
			SceneManager.LoadScene($"Level{nextLevelNumber}");
	}

	public void ReloadLevel()
	{
		SceneManager.LoadScene($"Level{currentLevel}");
	}

	public void Exit()
	{
		SceneManager.LoadScene("MainMenu");
	}
}