using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	[SerializeField] private CharacterController2D controller;
	[SerializeField] private Animator animator;
	[SerializeField] private AudioSource audioSource;
	[SerializeField] private AudioClip stepClip;
	[SerializeField] private AudioClip deathClip;
	[SerializeField] private AudioClip catchedClip;
	[SerializeField] private AudioClip jumpClip;

	[SerializeField] private float runSpeed = 40f;
	[SerializeField] private Vector2 stepTimeInterval;
	private float _horizontalMove;
	private bool _jump = false;
	private bool _dead = false;

	private float timer = 0f;
	private float timeToStep = 0f;

	// Update is called once per frame
	private void Update()
	{
		if(_dead) return;

		_horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
		animator.SetFloat("Moving", Mathf.Abs(_horizontalMove));

		if(Input.GetButtonDown("Jump"))
		{
			_jump = true;
			animator.SetTrigger("IsJumping");
			audioSource.PlayOneShot(jumpClip);
		}

		if(Mathf.Abs(_horizontalMove) > 0 && timer >= timeToStep && controller.IsGrounded)
		{
			timeToStep = Random.Range(stepTimeInterval.x, stepTimeInterval.y);
			timer = 0;
			audioSource.PlayOneShot(stepClip);
		}
		timer += Time.deltaTime;
	}

	private void FixedUpdate()
	{
		if(!_dead)
			controller.Move(_horizontalMove * Time.fixedDeltaTime, false, _jump);
		else
			controller.Move(0, false, false);
		_jump = false;
	}

	public void Death()
	{
		if(_dead) return;

		_dead = true;
		animator.SetTrigger("Dead");
		audioSource.PlayOneShot(deathClip);
	}

	public void Catched()
	{
		if(_dead) return;

		_dead = true;
		audioSource.PlayOneShot(catchedClip);
	}

	public void StopMoving()
	{
		_dead = true;
	}
}