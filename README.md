## It's not a penalty, it's a challenge

This project was developed during engineering studies as an implementation of the subject 2D games programming.

# Main objectives

* Plot should relate to the title of the game
* Include a basic tutorial
* Sound design in any form
* Enemy in any form

# Game

![Diagram](Screenshots/Screenshot-18.png)

![Diagram](Screenshots/Screenshot-31.png)

![Diagram](Screenshots/Screenshot-36.png)

![Diagram](Screenshots/Screenshot-11.png)